import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

//Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'

/*
createRoot - assigns the elements to be managed by React with its virtual DOM

render() - display the react element/components
App component is our mother component, this is the component we use as an entry
point and where we can render all other components or pages.

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);*/

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
const name = 'John Smith'
const element = <h1>Hello, {name}</h1>

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(element);
*/