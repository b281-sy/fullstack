//Allows us to navigate from one page of our app to another
import { Navigate } from "react-router-dom";

import { useContext, useEffect } from "react";

import UserContext from "../UserContext";



export default function Logout(){
        //allows us to clear all the data in the local storage ensuring that no data is stored in our browrser      
        // localStorage.clear();

        const {unsetUser, setUser} = useContext(UserContext);

        useEffect(() => {
            unsetUser();
            //since we cleared the contents of our local storage using the unsetUser() therefore the value of userState will be null;
            setUser({
                id: null,
                isAdmin: null
            });    
        }, [])
        
        return (
            <Navigate to = '/login' />
        )
}

