import { Fragment, useState, useEffect } from "react";
import CourseCard from "../components/CourseCard";
import coursesData from "../data/coursesData";

export default function Courses(){
    // Check to see if the mock data was captured
    // console.log(coursesData);

    // We are going to add a useEffect here so that everytime we refresh our app, it will fetch the updated content of our courses.

    const [courses, setCourses] = useState([]);

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/courses/`)
      .then(response => response.json())
      .then(data => {
        setCourses(data.map(course => {
          return(
            <CourseCard key = {course._id} courseProp = {course}/>
            )
        }))
      })
    }, [])
    /*  - The map method loops through the individual course objects in our array and returns a component for each course
        - Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed
        - Everytime the map method loops through the data, it creates a CourseCard component and then passes the current element in our courseData array using the courseProp
    */
  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });

    /* The course in the CourseCard Component is called a prop which is a shorthand for
    "property" since components are considered as objects in ReactJS
    
    The curly braces ({}) are used for props to signify that we are providing information
    using JavaScript expressions rather than hard coded values which is double ""

    We can pass information from one component to another using props. This is referred to 
    props drilling

    */
    return <Fragment>
      {courses}
    </Fragment>;
}